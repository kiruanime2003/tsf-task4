# Steps of setup and essentials of AWS EC2:

1. Sign up for a root user account or sign in using a existing root user account at https://aws.amazon.com/
2. Make sure you have verified your Payment method
3. Create a EC2 instance in AWS
    - Search for EC2 service and navigate to EC2 dashboard
    - Click on Launch instance button
    - Choose Windows_Server-2022-2022.09.14 AMI
    - Choose t2.micro instance type
    - Navigate to Network settings section and click on Create security group
    - Type 8GB for storage
    - Choose SSD volume type
    - Navigate to Advanced details section and click on Create new IAM profile
    - A new tab will be opened
    - Click on Create role button
    - Choose AWS service for Trusted entity type
    - Check AmazonEC2RoleforSSM
    - Type your role name under Role details
    - Click on Create role button
    - Now choose the created role under IAM instance profile
    - Click on create key pair
    - Choose a key pair name, type and private key file format
    - Click on create key pair button
    - Save your key pair file
    - Click on Launch instance button
    - Navigate to instances dashboard to view your instance
4. Connect to the instance
    - Select the instance
    - Click on Connect button
    - Navigate to RDP client tab
    - Download the remote desktop file
    - Click on get password and get the password
    - Open the remote desktop file


    
